import { DomHandlerOptions, DomHandler } from './dom'

export {
  DomHandlerOptions,
  DomHandler
}
