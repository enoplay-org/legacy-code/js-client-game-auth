
class DomHandlerOptions { }

// DomHandler handles interaction with the document object
class DomHandler {
  options: DomHandlerOptions
  constructor (options: DomHandlerOptions) {
    this.options = options
  }

  updateElementSrc (id: string, src: string) {
    let element = document.getElementById(id)
    if (!element) {
      return
    }
    element.setAttribute('src', src)
  }

  updateElementHref (id: string, href: string) {
    let element = document.getElementById(id)
    if (!element) {
      return
    }
    element.setAttribute('href', href)
  }

  updateElementStyle (id: string, style: string) {
    let element = document.getElementById(id)
    if (!element) {
      return
    }
    element.setAttribute('style', style)
  }

  updateElementInnerHTML (id: string, innerHTML: string) {
    let element = document.getElementById(id)
    if (!element) {
      return
    }
    element.innerHTML = innerHTML
  }
}

export {
  DomHandlerOptions,
  DomHandler
}
