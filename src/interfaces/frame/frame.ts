
class FrameHandlerOptions {
  // wwwHostURL is the domain of the www version of the parent frame
  wwwHostURL: string

  // mobileHostURL is the domain of the mobile version of the parent frame
  mobileHostURL: string

  // betaHostURL is the domain of the beta version of the parent frame
  betaHostURL: string

  constructor (wwwHostURL: string, mobileHostURL: string, betaHostURL: string) {
    this.wwwHostURL = wwwHostURL
    this.mobileHostURL = mobileHostURL
    this.betaHostURL = betaHostURL
  }
}

// FrameHandler handles the communication between the current window and the parent frame
class FrameHandler {
  options: FrameHandlerOptions
  messageHandlers: { [key: string]: (data: any) => void }

  constructor (options: FrameHandlerOptions) {
    this.options = options
    this.messageHandlers = {}
  }

  // listenForMessage starts listening for messages from the parent frame
  listenForMessage () {
    // Ensure the current window has a parent frame
    if (!isWindowInFrame()) {
      return
    }

    console.log(`Listening for messages from parent...`)
    window.addEventListener('message', this.handleMessage.bind(this))
  }

  // sendMessage sends a message to the parent frame
  sendMessage (message: string, description: string) {
    let parentOrigin: string = stripURLPath(document.referrer.slice(0, -1))

    if (!(parentOrigin === this.options.wwwHostURL ||
      parentOrigin === this.options.mobileHostURL || parentOrigin === this.options.betaHostURL)) {
      console.warn(`Error sending message to unexpected origin: ${parentOrigin}`)
      return
    }

    parent.postMessage({
      message,
      description
    }, parentOrigin)
  }

  // handleMessage handles a message received from the parent frame
  handleMessage (event: MessageEvent) {
    let origin: string = stripURLPath(event.origin)
    let data: { [key: string]: any } = event.data

    // Ensure the origin is a parent frame
    if (!(origin === this.options.wwwHostURL ||
      origin === this.options.mobileHostURL || origin === this.options.betaHostURL)) {
      console.warn(`Error receiving message from unexpected origin: ${origin}`)
      return
    }

    if (!this.messageHandlers[data.message]) {
      console.warn(`Error finding message handler for: ${data.message}`)
      return
    }

    this.messageHandlers[data.message](data)
  }

  addMessageHandler (message: string, handler: (data: any) => void) {
    this.messageHandlers[message] = handler
  }

  // scheduleMessage sends a message to the parent frame every period length (measured in minutes)
  scheduleMessage (message: string, description: string, period: number) {
    window.setInterval(() => {
      this.sendMessage(message, description)
    }, period * 1000 * 60)
  }
}

// isWindowInFrame checks if the current window is in a frame
function isWindowInFrame (): boolean {
  return window.self !== window.top
}

function stripURLPath (url: string): string {
  let parser = document.createElement('a')
  parser.href = url
  return `${parser.protocol}//${parser.host}`
}

export {
  FrameHandlerOptions,
  FrameHandler
}
