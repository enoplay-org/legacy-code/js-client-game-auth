import axios from 'axios'

class PlayTokenRepoOptions {
  tokenVerifyURL: string
  constructor (tokenVerifyURL: string) {
    this.tokenVerifyURL = tokenVerifyURL
  }
}

class PlayTokenRepo {
  options: PlayTokenRepoOptions

  constructor (options: PlayTokenRepoOptions) {
    this.options = options
  }

  // verify checks if a given token is authorized to play the current game
  verify (token: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return axios({
        method: 'post',
        url: this.options.tokenVerifyURL,
        withCredentials: true,
        data: {
          token
        }
      })
      // Ensure the response is valid
      .then((response) => {
        let data = response.data
        if (!data) {
          return Promise.reject(`Error determining verification response data in repoToken`)
        }
        return data.success
      })
      // Respond
      .then((isVerified) => {
        resolve(isVerified)
      })
      .catch((err) => {
        reject(`Error verifying user play token: ${err}`)
      })
    })
  }
}

export {
  PlayTokenRepoOptions,
  PlayTokenRepo
}
