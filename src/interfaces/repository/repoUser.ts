import axios from 'axios'

import { User } from '../../domain'

class UserRepoOptions {
  apiHostURL: string
  constructor (apiHostURL: string) {
    this.apiHostURL = apiHostURL
  }
}

class UserRepo {
  options: UserRepoOptions
  constructor (options: UserRepoOptions) {
    this.options = options
  }

  getByUID (uid: string): Promise<User> {
    return new Promise((resolve, reject) => {
      return axios.get(`${this.options.apiHostURL}/users?field=uid&q=${uid}`)
      .then(response => {
        let data = response.data

        if (!data.success) {
          return Promise.reject(`Error: Expected response from apiHost`)
        }

        let user = data.users[0]
        return user
      })
      .then(userResponse => {
        resolve(userResponse)
      })
      .catch(err => {
        reject(`Error retrieving user from apiHost: ${this.options.apiHostURL}\n\n${err}`)
      })
    })
  }
}

export {
  UserRepoOptions,
  UserRepo
}
