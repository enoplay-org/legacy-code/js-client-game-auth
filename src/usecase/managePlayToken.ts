import { PlayTokenRepository, PlayTokenClaim } from '../domain'

class PlayTokenInteractorOptions {
  isAuthIndex: boolean

  constructor (isAuthIndex: boolean) {
    this.isAuthIndex = isAuthIndex
  }
}

class PlayTokenInteractor {
  options: PlayTokenInteractorOptions
  playTokenRepository: PlayTokenRepository

  constructor (options: PlayTokenInteractorOptions) {
    this.options = options
  }

  setPlayTokenRepository (playTokenRepository: PlayTokenRepository) {
    this.playTokenRepository = playTokenRepository
  }

  verify (claim: PlayTokenClaim): Promise<boolean> {
    return this.playTokenRepository.verify(claim.jti)
    .then((isVerified) => {
      console.log(`Verification complete: ${ isVerified ? 'User is verified' : 'User is not verified' }`)

      // Reload the current window
      if (isVerified && this.options.isAuthIndex) {
        location.reload()
      }

      return isVerified
    })
    .catch(err => {
      console.warn(`Error verifying token: ${err}`)
      return Promise.resolve(false)
    })
  }
}

export {
  PlayTokenInteractorOptions,
  PlayTokenInteractor
}
