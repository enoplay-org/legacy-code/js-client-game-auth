interface PlayTokenRepository {
  verify (token: string): Promise<boolean>
}

class PlayTokenClaim {
  jti: string
  constructor (jti: string) {
    this.jti = jti
  }
}

export {
  PlayTokenRepository,
  PlayTokenClaim
}
