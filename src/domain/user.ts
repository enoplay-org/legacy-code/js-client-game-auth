interface UserRepository {
  getByUID (uid: string): Promise<User>
}

class User {
  uid: string
  username: string
  alias: string
  media: UserMedia
}

interface UserMedia {
  icon: { source: string }
}

export {
  UserRepository,
  User
}
