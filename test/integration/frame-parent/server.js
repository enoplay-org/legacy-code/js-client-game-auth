var path = require('path')
var express = require('express')
var cors = require('cors')

var server = express()

server.use(express.static(__dirname))

server.get('/*', function (req, res) {
  res.sendFile(path.resolve(__dirname, './index.html'))
})

var port = process.env.PORT || 7700
server.listen(port, function () {
  console.log('parent frame: server listening on port ' + port)
})
