const path = require('path')
const webpack = require('webpack')
var config = require('../config')

module.exports = {
  context: path.resolve(__dirname, '../src'),
  entry: {
    app: './main.ts',
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: config.build.fileName,
  },
  module: {
    rules: [
      {
       enforce: 'pre',
       test: /\.js$/,
       loader: "source-map-loader"
      },
      {
        enforce: 'pre',
        test: /\.ts$/,
        use: "source-map-loader"
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [ 'babel-loader', 'ts-loader' ],
      }
    ]
  },
  plugins:[
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false,
      }
    })
  ],
  resolve: {
    extensions: ['.ts', '.js']
  }
}
