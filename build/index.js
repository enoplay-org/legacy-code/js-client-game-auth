// https://github.com/shelljs/shelljs
require('./check-versions')()
require('shelljs/global')
env.NODE_ENV = 'production'

var path = require('path')
var config = require('../config')
var ora = require('ora')
var webpack = require('webpack')
var webpackConfig = require('./webpack.base')

var spinner = ora('building for production...')
spinner.start()

var distPath = config.build.distPath
rm('-rf', distPath)
mkdir('-p', distPath)

webpack(webpackConfig, function (err, stats) {
  spinner.stop()
  if (err) throw err
  process.stdout.write(stats.toString({
    colors: true,
    modules: false,
    children: false,
    chunks: false,
    chunkModules: false
  }) + '\n')

  // Generate copy in test paths
  for (var i = 0; i < config.build.testPaths.length; i++) {
    cp('-R', path.resolve(config.build.distPath, config.build.fileName), config.build.testPaths[i])
  }
})
